#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "server/server.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();


    public slots:

        void StartAndStopServer();

    private:

        Ui::MainWindow *ui;
        Server *mServer;

};

#endif // MAINWINDOW_H
