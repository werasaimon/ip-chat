#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("IPChat - server");
    connect( ui->pushButtonStartServer,SIGNAL(clicked()), this, SLOT(StartAndStopServer()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::StartAndStopServer()
{
    if(!mServer)
    {
        mServer = new Server();
        mServer->SetTextEdit(ui->messages);
        mServer->LisenPort(1024);

        if(mServer->isLisenPort())
        {
           ui->pushButtonStartServer->setText("stop server");
        }
        else
        {
            delete mServer;
            mServer = nullptr;
        }
    }
    else
    {
        mServer->onMessage("wera");

        delete mServer;
        mServer = nullptr;

        ui->pushButtonStartServer->setText("start server");
        ui->messages->append("stop server");
    }
}
