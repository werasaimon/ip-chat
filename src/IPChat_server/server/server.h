#ifndef SERVER_H
# define SERVER_H
# include <QObject>
# include <QList>

#include <QTextEdit>

class QTcpServer;
class QTcpSocket;
class ISocketAdapter;

class Server : public QObject {
  Q_OBJECT
public:
  explicit Server(QObject *parent = 0);
   ~Server();

  bool isLisenPort() const;

public slots:

  void LisenPort( int nPort );

  void onNewConnection();
  void onDisconnected();
  void onMessage(QString);

  void SetTextEdit(QTextEdit *_pTextEditServer);

protected:

  QTcpServer* m_pTCPServer;
  QList<ISocketAdapter*> m_Clients;

  bool m_isLisenPort;

   QTextEdit *m_pMessages;
};

#endif // SERVER_H
