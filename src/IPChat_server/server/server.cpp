#include "server.h"
#include "serversocketadapter.h"

#include <QTcpServer>
#include <QTcpSocket>
#include <QString>

Server::Server( QObject *parent) : QObject(parent),
  m_pTCPServer(new QTcpServer(this)) {

  connect(m_pTCPServer, SIGNAL(newConnection()), SLOT(onNewConnection()));

}

Server::~Server()
{
    if(m_pTCPServer)
    {
        delete m_pTCPServer;
        m_pTCPServer = nullptr;
    }

    for( auto item : m_Clients )
    {
       delete item;
       item = nullptr;
    }

    m_Clients.clear();
}

void Server::LisenPort(int nPort)
{

    QHostAddress host = QHostAddress::LocalHost;

    if (m_pTCPServer->listen(host, nPort) == false)
    {
        m_isLisenPort = false;
        m_pTCPServer->close();
        throw m_pTCPServer->errorString();
    }
    else
    {
        m_pMessages->append(QString("Start-Server IP:") + host.toString());
        m_isLisenPort = true;
    }

}


void Server::onNewConnection()
{
  QTextStream(stdout) << "new connection" << endl;

  QTcpSocket* pclientSock = m_pTCPServer->nextPendingConnection();
  ISocketAdapter *pSockHandle = new ServerSocketAdapter(pclientSock);

  m_pMessages->append( QString("new connection IP:") + pclientSock->peerAddress().toString() );


  m_Clients.push_back(pSockHandle);

  pSockHandle->SendString(" messages from server : connect ");

  connect(pSockHandle, SIGNAL(disconnected()), SLOT(onDisconnected()));
  connect(pSockHandle, SIGNAL(message(QString)), SLOT(onMessage(QString)));
}

void Server::onDisconnected()
{
  QTextStream(stdout) << "client disconnected" << endl;

  m_pMessages->append("client disconnected");

  ISocketAdapter* client = static_cast<ServerSocketAdapter*>(sender());
  m_Clients.removeOne(client);
  delete client;
}

void Server::onMessage(QString msg)
{
  foreach (ISocketAdapter *sock, m_Clients)
  {
    sock->SendString(msg);
  }
}

void Server::SetTextEdit(QTextEdit *_pTextEditServer)
{
    m_pMessages = _pTextEditServer;
}

bool Server::isLisenPort() const
{
    return m_isLisenPort;
}



