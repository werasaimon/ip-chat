#include "clientsocketadapter.h"
#include <QVector>
#include <QTcpSocket>
#include <QDataStream>

#include <QAbstractSocket>

ClientSocketAdapter::ClientSocketAdapter( QObject *parent , const QString &ip , int port)
  : SocketAdapter(parent) , m_isConnectionToServer(false)
{
  m_pTCPSocket->connectToHost(ip, port);

  if (m_pTCPSocket->waitForConnected(1000))
  {
      qDebug("Connected!");
      m_isConnectionToServer = true;
  }
  else
  {
     qDebug("No Connected!");
     m_isConnectionToServer = false;
  }
}

ClientSocketAdapter::~ClientSocketAdapter()
{
    if(m_pTCPSocket && m_isConnectionToServer)
    {
        m_pTCPSocket->disconnectFromHost();
        if (m_pTCPSocket->state() == QAbstractSocket::UnconnectedState || m_pTCPSocket->waitForDisconnected(1000))
        {
            qDebug("Disconnected!");
        }
    }
}

bool ClientSocketAdapter::isConnectionToServer() const
{
    return m_isConnectionToServer;
}


