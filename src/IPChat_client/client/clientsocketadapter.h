#ifndef CLIENTSOCKETADAPTER_H
# define CLIENTSOCKETADAPTER_H

# include "../common/socketadapter.h"

class ClientSocketAdapter : public SocketAdapter
{
        Q_OBJECT

    public:

        explicit ClientSocketAdapter( QObject *parent , const QString &ip = "localhost" , int port = 1024 );

        ~ClientSocketAdapter();

        bool isConnectionToServer() const;

    private:

        bool m_isConnectionToServer;


};

#endif // CLIENTSOCKETADAPTER_H
