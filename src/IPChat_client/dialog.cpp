#include "dialog.h"
#include "ui_dialog.h"

#include "client/clientsocketadapter.h"

#include <QScrollBar>
#include <QtDebug>

Dialog::Dialog(QWidget *parent) :
QDialog(parent),
ui(new Ui::Dialog)
{
    ui->setupUi(this);

    this->setWindowTitle("IPChat - client");
    connect(ui->send, SIGNAL(clicked()), SLOT(onSend()));
    connect(ui->pushButtonIPConnect , SIGNAL(clicked()) , this , SLOT(onConnect()));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::onMessage(QString text)
{
    ui->messages->setHtml(ui->messages->toHtml() + text + "<br>");
    ui->messages->verticalScrollBar()->setSliderPosition(ui->messages->verticalScrollBar()->maximum());
}

void Dialog::onSend()
{

    if(m_pSocket)
    {
       m_pSocket->SendString(ui->message->text());
       ui->message->clear();
    }
}

void Dialog::onConnect()
{
    if(!m_pSocket)
    {
        int port = 1024;
        QString EditIPText = ui->lineEdit_IP->text();
        m_pSocket = new ClientSocketAdapter(this , EditIPText , port );
        connect(m_pSocket, SIGNAL(message(QString)), SLOT(onMessage(QString)));

        if(m_pSocket->isConnectionToServer())
        {
            ui->pushButtonIPConnect->setText("IP-Disconect");
        }
        else
        {
            ui->messages->append("no connection");
            delete m_pSocket;
            m_pSocket = nullptr;
        }
    }
    else
    {
        delete m_pSocket;
        m_pSocket = nullptr;
        ui->pushButtonIPConnect->setText("IP-Connect");
    }
}
