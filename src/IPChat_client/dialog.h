#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui
{
    class Dialog;
}

class ISocketAdapter;
class Dialog : public QDialog
{
        Q_OBJECT

    public:
        explicit Dialog(QWidget *parent = nullptr);
        ~Dialog();

    private:


    public slots:

        void onMessage(QString text);
        void onSend();
        void onConnect();

    protected:

        Ui::Dialog *ui;
        ISocketAdapter *m_pSocket;
};

#endif // DIALOG_H
