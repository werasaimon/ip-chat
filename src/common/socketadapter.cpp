#include "socketadapter.h"

#include <QVector>
#include <QTcpSocket>
#include <QDataStream>
#include <QAbstractSocket>

SocketAdapter::SocketAdapter(QObject *parent, QTcpSocket *pSock)
  : ISocketAdapter(parent), m_MsgSize(-1)
{
    if (!pSock)
    {
      m_pTCPSocket = new QTcpSocket(this);
    }
    else
    {
      m_pTCPSocket = pSock;
    }

    connect(m_pTCPSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(m_pTCPSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}

SocketAdapter::~SocketAdapter()
{
    if(!m_pTCPSocket)
    {
        onDisconnected();
        delete m_pTCPSocket;
        m_pTCPSocket = nullptr;
    }
}

void SocketAdapter::onReadyRead()
{
  QString buff;
  QDataStream stream(m_pTCPSocket);

  while(true)
  {
    if (m_MsgSize < 0)
    {
      if (m_pTCPSocket->bytesAvailable() < (qint64)sizeof(int)) return;

      stream >> m_MsgSize;
    }
    else
    {
      if (m_pTCPSocket->bytesAvailable() < m_MsgSize) return;
      stream >> buff;

      emit message(buff);
      m_MsgSize = -1;
    }
  }
}

void SocketAdapter::SendString(const QString& str)
{
  QByteArray block;
  QDataStream sendStream(&block, QIODevice::ReadWrite);

  sendStream << quint16(0) << str;

  sendStream.device()->seek(0);
  sendStream << (quint16)(block.size() - sizeof(quint16));


  m_pTCPSocket->write(block);
}



void SocketAdapter::onDisconnected()
{
    m_pTCPSocket->deleteLater();
    emit disconnected();
}






