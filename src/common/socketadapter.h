#ifndef SOCKETADAPTER_H
# define SOCKETADAPTER_H

# include "isocketadapter.h"

class QTcpSocket;
class SocketAdapter : public ISocketAdapter
{
  Q_OBJECT

public:

  SocketAdapter(QObject *parent, QTcpSocket *pSock = 0);
  virtual ~SocketAdapter();
  virtual void SendString(const QString& str);

protected slots:

  void onReadyRead();
  void onDisconnected();

protected:
  QTcpSocket *m_pTCPSocket;
  qint16 m_MsgSize;
};

#endif // SOCKETADAPTER_H
